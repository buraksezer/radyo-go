# Radyo

Distributed publish-subscribe server with WebSocket support.

## WIP

This project is a work in progress. The implementation is incomplete. The documentation may be inaccurate.

## Contributions

Please don't hesitate to fork the project and send a pull request or just e-mail me to ask questions and share ideas.

## License

MIT License, - see LICENSE for more details.
