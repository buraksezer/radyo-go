// Copyright 2017 Burak Sezer
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package radyo

import (
	"bytes"
	"errors"
	"net/http"
	"time"

	"github.com/rs/zerolog/log"
)

var errAuthentication = errors.New("Authentication failed")

type authHandler struct {
	Handler     http.Handler
	timeout     time.Duration
	callbackURL string
}

func (a *authHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method == "OPTIONS" {
		// Browser does not send authentication headers in OPTIONS request.
		a.Handler.ServeHTTP(w, r)
		return
	}
	if len(a.callbackURL) != 0 {
		ah := r.Header.Get("X-Radyo-Auth")
		if len(ah) == 0 {
			log.Debug().Msg("Missing header: X-Radyo-Auth")
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		code, err := a.authenticateClient(ah)
		if err != nil {
			log.Debug().Err(err).Msgf("Authentication failed. Status Code: %d", code)
			w.WriteHeader(code)
			return
		}
		log.Debug().Msgf("Authentication successfull. Status Code: ", code)
	}
	a.Handler.ServeHTTP(w, r)
}

func (a *authHandler) authenticateClient(credentials string) (int, error) {
	var query = []byte(credentials)
	req, err := http.NewRequest("POST", a.callbackURL, bytes.NewBuffer(query))
	client := &http.Client{
		Timeout: a.timeout,
	}
	req.Header.Set("Content-Type", "text/plain")
	resp, err := client.Do(req)
	if err != nil {
		return http.StatusInternalServerError, err
	}
	if resp.StatusCode != http.StatusOK {
		return resp.StatusCode, errAuthentication
	}
	defer func() {
		if err := resp.Body.Close(); err != nil {
			log.Error().Err(err).Msg("Error while closing response body.")
		}
	}()
	return resp.StatusCode, nil
}
