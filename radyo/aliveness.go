// Copyright 2017 Burak Sezer
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package radyo

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"time"

	"github.com/julienschmidt/httprouter"
	"github.com/rs/zerolog/log"
)

// alivenessHandler is a dummy endpoint which just returns HTTP 204 to caller. It can be used to check server aliveness.
func (r *Radyo) alivenessHandler(w http.ResponseWriter, _ *http.Request, _ httprouter.Params) {
	w.WriteHeader(http.StatusNoContent)
}

func (r *Radyo) checkAliveness(servers []*http.Server) error {
	check := func(server *http.Server) error {
		target := url.URL{
			Host:   server.Addr,
			Scheme: "http",
			Path:   "/aliveness",
		}
		if r.tlsEnabled {
			target.Scheme = "https"
		}

		req, err := http.NewRequest(http.MethodHead, target.String(), nil)
		if err != nil {
			return err
		}
		// Use local context for request cancellation.
		req = req.WithContext(r.ctx)
		resp, err := r.client.Do(req)
		if err != nil {
			return err
		}

		defer func() {
			if err := resp.Body.Close(); err != nil {
				log.Error().Err(err).Msg("Error while closing response body")
			}
		}()
		if resp.StatusCode != http.StatusNoContent {
			r, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				return err
			}
			return fmt.Errorf("%s returned an unexpected return code %d: %s",
				server.Addr, resp.StatusCode, string(r))
		}
		return nil
	}

	var err error
	maxRetry := 20
	for _, srv := range servers {
		idx := 0
		for idx < maxRetry {
			err = check(srv)
			if err != nil {
				idx++
				<-time.After(50 * time.Millisecond)
				continue
			}
			break
		}
		if idx >= maxRetry {
			break
		}
	}
	return err
}
