// Copyright (c) 2018 Burak Sezer
// All rights reserved.
//
// This code is licensed under the MIT License.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files(the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions :
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package radyo

import (
	"context"
	"crypto/tls"
	"net"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"golang.org/x/net/http2"
	"golang.org/x/sync/errgroup"

	"github.com/buraksezer/radyo/config"
	"github.com/julienschmidt/httprouter"
	"github.com/rs/zerolog/log"
)

const gracePeriod = 5000 * time.Millisecond

// Radyo represents a new Radyo instance.
type Radyo struct {
	tlsEnabled   bool
	internalSrv  *http.Server
	websocketSrv *http.Server
	discovery    *discovery
	client       *http.Client
	config       *config.Config
	errGroup     errgroup.Group
	ctx          context.Context
	cancel       context.CancelFunc
}

func newHTTP2Client(c *config.Config) (*http.Client, error) {
	dialerTimeout, err := time.ParseDuration(c.HTTPClient.DialerTimeout)
	if err != nil {
		return nil, err
	}
	timeout, err := time.ParseDuration(c.HTTPClient.Timeout)
	if err != nil {
		return nil, err
	}
	tc := &tls.Config{InsecureSkipVerify: c.HTTPClient.InsecureSkipVerify}
	dialTLS := func(network, addr string, cfg *tls.Config) (net.Conn, error) {
		d := &net.Dialer{Timeout: dialerTimeout}
		return tls.DialWithDialer(d, network, addr, cfg)
	}
	return &http.Client{
		Transport: &http2.Transport{
			DialTLS:         dialTLS,
			TLSClientConfig: tc,
		},
		Timeout: timeout,
	}, nil
}

func New(c *config.Config) (*Radyo, error) {
	ctx, cancel := context.WithCancel(context.Background())
	r := &Radyo{
		config: c,
		ctx:    ctx,
		cancel: cancel,
	}
	if r.config.Radyo.CertFile != "" || r.config.Radyo.KeyFile != "" {
		r.tlsEnabled = true
	}

	if r.tlsEnabled {
		client, err := newHTTP2Client(c)
		if err != nil {
			return nil, err
		}
		r.client = client
	} else {
		// TODO: make a function to create an HTTP client with configuration.
		r.client = &http.Client{}
	}

	internalRouter := httprouter.New()
	clientRouter := httprouter.New()

	// Register dummy endpoints for aliveness check.
	clientRouter.HEAD("/aliveness", r.alivenessHandler)
	internalRouter.HEAD("/aliveness", r.alivenessHandler)

	atimeout, err := time.ParseDuration(r.config.Radyo.AuthTimeout)
	if err != nil {
		return nil, err
	}
	handler := &authHandler{
		Handler:     clientRouter,
		timeout:     atimeout,
		callbackURL: r.config.Radyo.AuthCallbackUrl,
	}
	r.websocketSrv = &http.Server{
		Addr:    r.config.Radyo.ClientAddr,
		Handler: handler,
	}

	r.internalSrv = &http.Server{
		Addr:    r.config.Radyo.InternalAddr,
		Handler: internalRouter,
	}
	return r, nil
}

func (r *Radyo) listenAndServe(srv *http.Server) error {
	if srv.Addr == r.config.Radyo.ClientAddr {
		log.Info().Str("addr", srv.Addr).Bool("tls", r.tlsEnabled).Msg("Listening client connections")
	} else if srv.Addr == r.config.Radyo.InternalAddr {
		log.Info().Str("addr", srv.Addr).Bool("tls", r.tlsEnabled).Msg("Listening internal connections")
	}

	var err error
	if r.tlsEnabled {
		err = srv.ListenAndServeTLS(r.config.Radyo.CertFile, r.config.Radyo.KeyFile)
	} else {
		err = srv.ListenAndServe()
	}
	if err != nil && err != http.ErrServerClosed {
		if opErr, ok := err.(*net.OpError); !ok || (ok && opErr.Op != "accept") {
			log.Debug().Err(err).Msg("Something wrong with HTTP server")
			// We call Close here because if HTTP server fails, we cannot run this process anymore.
			// This block can be invoked during normal closing process but Close method will handle
			// redundant Close call.
			r.Shutdown()
		}
		log.Error().Err(err).Str("addr", srv.Addr).Msgf("Error while running HTTP server")
		return err
	}
	return nil
}

// Start starts a new Radyo server instance and blocks until the server is closed.
func (r *Radyo) Start() error {
	// Wait for SIGTERM or SIGINT
	go r.waitForInterrupt()

	servers := []*http.Server{r.websocketSrv, r.internalSrv}
	for _, server := range servers {
		srv := server
		r.errGroup.Go(func() error {
			return r.listenAndServe(srv)
		})
	}

	// Be sure that all the HTTP servers are up. Another components depends on HTTP servers.
	err := r.checkAliveness(servers)
	if err != nil {
		// Close the server gracefully.
		r.Shutdown()
		return err
	}

	r.discovery, err = newDiscovery(r.config)
	if err != nil {
		return err
	}
	// Try to join the cluster by using a pre-defined peer list which
	// is defined in config file.
	r.discovery.join()

	// Now, Radyo is up and running. Wait until shutdown.
	<-r.ctx.Done()

	log.Info().Msg("Shutting down Radyo node.")
	for _, server := range servers {
		srv := server
		r.errGroup.Go(func() error {
			d := time.Now().Add(gracePeriod)
			ctx, cancel := context.WithDeadline(context.Background(), d)
			defer cancel()
			err := srv.Shutdown(ctx)
			if err != nil {
				log.Error().
					Err(err).
					Str("addr", srv.Addr).Msg("Failed to shutdown HTTP server")
			}
			return err
		})
	}
	return r.errGroup.Wait()
}

func (r *Radyo) waitForInterrupt() {
	shutDownChan := make(chan os.Signal)
	signal.Notify(shutDownChan, syscall.SIGTERM, syscall.SIGINT)
	s := <-shutDownChan
	log.Info().Str("signal", s.String()).Msgf("Signal catched")
	r.Shutdown()
}

// Shutdown signals Radyo instance to close.
func (r *Radyo) Shutdown() {
	select {
	case <-r.ctx.Done():
		return
	default:
	}

	r.cancel()

	// Use errGroup to shut down discovery package because it blocks
	// until the memberlist is stopped and we need to show and control
	// the errors at end of the program.
	r.errGroup.Go(func() error {
		err := r.discovery.shutdown()
		if err != nil {
			log.Error().Err(err).Msg("Failed to shutdown discovery subsystem")
		}
		return err
	})
}
