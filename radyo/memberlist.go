// Copyright 2017 Burak Sezer
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package radyo

import (
	"bytes"
	"fmt"
	stdlog "log"
	"net"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/buraksezer/radyo/config"
	"github.com/hashicorp/memberlist"
	"github.com/rs/zerolog/log"
)

type mLogger struct {
	debug, warn, info, err []byte
}

func newMLogger() *mLogger {
	return &mLogger{
		debug: []byte("[DEBUG]"),
		info:  []byte("[INFO]"),
		err:   []byte("[ERR]"),
		warn:  []byte("[WARN]"),
	}
}

func (t mLogger) Write(p []byte) (n int, err error) {
	p = bytes.TrimSpace(p)
	switch {
	case bytes.Equal(t.debug, p[:len(t.debug)]):
		p = bytes.TrimSpace(p[len(t.debug):])
		log.Debug().Msg(string(p))
	case bytes.Equal(t.info, p[:len(t.info)]):
		p = bytes.TrimSpace(p[len(t.info):])
		log.Info().Msg(string(p))
	case bytes.Equal(t.err, p[:len(t.err)]):
		p = bytes.TrimSpace(p[len(t.err):])
		log.Error().Msg(string(p))
	case bytes.Equal(t.warn, p[:len(t.warn)]):
		p = bytes.TrimSpace(p[len(t.warn):])
		log.Warn().Msg(string(p))
	}
	return len(p), nil
}

// newMemberlistConf creates a new *memberlist.Config by parsing olricmq.toml.
func newMemberlistConf(c *config.Config, eventsCh chan memberlist.NodeEvent, birthdate int64) (*memberlist.Config, error) {
	bindAddr, sport, err := net.SplitHostPort(c.Discovery.Addr)
	if err != nil {
		return nil, err
	}
	bindPort, err := strconv.Atoi(sport)
	if err != nil {
		return nil, err
	}

	var mc = &memberlist.Config{}
	switch {
	case c.Discovery.Environment == "local":
		mc = memberlist.DefaultLocalConfig()
	case c.Discovery.Environment == "lan":
		mc = memberlist.DefaultLANConfig()
	case c.Discovery.Environment == "wan":
		mc = memberlist.DefaultWANConfig()
	default:
		return nil, fmt.Errorf("discovery.environment is missing or invalid.")
	}
	dlg, err := newDelegate(birthdate)
	if err != nil {
		return nil, err
	}

	_, port, err := net.SplitHostPort(c.Radyo.ClientAddr)
	if err != nil {
		return nil, err
	}

	hostname, err := os.Hostname()
	if err != nil {
		return nil, err
	}
	name := strings.Join(
		[]string{
			strconv.Itoa(os.Getpid()),
			strconv.FormatInt(time.Now().Unix(), 10),
			hostname,
		}, ".")
	mc.Name = net.JoinHostPort(name, port)
	mc.BindAddr = bindAddr
	mc.BindPort = bindPort
	mc.EnableCompression = c.Discovery.EnableCompression
	mc.Delegate = dlg
	mc.Logger = stdlog.New(newMLogger(), "", 0)
	mc.Events = &memberlist.ChannelEventDelegate{
		Ch: eventsCh,
	}

	if len(c.Discovery.TCPTimeout) != 0 {
		mc.TCPTimeout, err = time.ParseDuration(c.Discovery.TCPTimeout)
		if err != nil {
			return nil, err
		}
	}

	if c.Discovery.IndirectChecks != 0 {
		mc.IndirectChecks = c.Discovery.IndirectChecks
	}

	if c.Discovery.RetransmitMult != 0 {
		mc.RetransmitMult = c.Discovery.RetransmitMult
	}

	if c.Discovery.SuspicionMult != 0 {
		mc.SuspicionMult = c.Discovery.SuspicionMult
	}

	if len(c.Discovery.PushPullInterval) != 0 {
		mc.PushPullInterval, err = time.ParseDuration(c.Discovery.PushPullInterval)
		if err != nil {
			return nil, err
		}
	}

	if len(c.Discovery.ProbeTimeout) != 0 {
		mc.ProbeTimeout, err = time.ParseDuration(c.Discovery.ProbeTimeout)
		if err != nil {
			return nil, err
		}
	}

	if len(c.Discovery.ProbeInterval) != 0 {
		mc.ProbeInterval, err = time.ParseDuration(c.Discovery.ProbeInterval)
		if err != nil {
			return nil, err
		}
	}

	if len(c.Discovery.GossipInterval) != 0 {
		mc.GossipInterval, err = time.ParseDuration(c.Discovery.GossipInterval)
		if err != nil {
			return nil, err
		}
	}

	if len(c.Discovery.GossipToTheDeadTime) != 0 {
		mc.GossipToTheDeadTime, err = time.ParseDuration(c.Discovery.GossipToTheDeadTime)
		if err != nil {
			return nil, err
		}
	}
	return mc, nil
}
