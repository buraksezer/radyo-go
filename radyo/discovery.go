// Copyright (c) 2018 Burak Sezer
// All rights reserved.
//
// This code is licensed under the MIT License.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files(the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions :
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package radyo

import (
	"bytes"
	"encoding/gob"
	"errors"
	"sort"
	"sync"
	"time"

	"github.com/buraksezer/radyo/config"
	"github.com/hashicorp/memberlist"
	"github.com/rs/zerolog/log"
)

const eventChanCapacity = 32

var errHostNotFound = errors.New("host not found")

// discovery is a structure that encapsulates memberlist and
// provides useful functions to utilize it.
type discovery struct {
	name       string
	peers      []string
	memberlist *memberlist.Memberlist
	config     *memberlist.Config
	birthdate  int64
	wg         sync.WaitGroup
	done       chan struct{}

	eventMutex       sync.RWMutex
	eventsChan       chan memberlist.NodeEvent
	eventSubscribers []chan memberlist.NodeEvent
}

// TODO: NodeMetadata will be removed.
type NodeMetadata struct {
	Birthdate int64
}

// host represents a node in the cluster.
type host struct {
	NodeMetadata
	Name string
}

func (m host) String() string {
	return m.Name
}

func (d *discovery) DecodeMeta(buf []byte) (*NodeMetadata, error) {
	res := &NodeMetadata{}
	r := bytes.NewReader(buf)
	if err := gob.NewDecoder(r).Decode(res); err != nil {
		return nil, err
	}
	return res, nil
}

// New creates a new memberlist with a proper configuration and returns a new discovery instance along with it.
func newDiscovery(c *config.Config) (*discovery, error) {
	birthdate := time.Now().UnixNano()
	eventsCh := make(chan memberlist.NodeEvent, eventChanCapacity)
	mc, err := newMemberlistConf(c, eventsCh, birthdate)
	if err != nil {
		return nil, err
	}
	list, err := memberlist.Create(mc)
	if err != nil {
		return nil, err
	}
	return &discovery{
		name:       mc.Name,
		birthdate:  birthdate,
		memberlist: list,
		peers:      c.Discovery.Peers,
		config:     mc,
		eventsChan: eventsCh,
		done:       make(chan struct{}),
	}, nil
}

// join is used to take an existing Memberlist and attempt to join a cluster
// by contacting all the given hosts and performing a state sync. Initially,
// the Memberlist only contains our own state, so doing this will cause remote
// nodes to become aware of the existence of this node, effectively joining the cluster.
func (d *discovery) join() {
	if len(d.peers) != 0 {
		nr, err := d.memberlist.Join(d.peers)
		if err != nil {
			log.Warn().Err(err).Msg("There are some errors")
		}
		if nr == 0 {
			log.Warn().Msg("Join failed. Running as standalone")
		} else {
			log.Info().Msgf("The number of hosts successfully contacted: %d", nr)
		}
	}
	d.wg.Add(1)
	go d.eventLoop()
	log.Info().Str("name", d.config.Name).Msg("New Radyo node has been created")
}

// getMembers returns a list of all known live nodes.
func (d *discovery) getMembers() []host {
	members := []host{}
	nodes := d.memberlist.Members()
	for _, node := range nodes {
		mt, _ := d.DecodeMeta(node.Meta)
		member := host{
			Name:         node.Name,
			NodeMetadata: *mt,
		}
		members = append(members, member)
	}

	// sort members by birthdate
	sort.Slice(members, func(i int, j int) bool {
		return members[i].Birthdate < members[j].Birthdate
	})
	return members
}

func (d *discovery) numMembers() int {
	return len(d.memberlist.Members())
}

// findMember finds and returns an alive member.
func (d *discovery) findMember(name string) (host, error) {
	members := d.getMembers()
	for _, member := range members {
		if member.Name == name {
			return member, nil
		}
	}
	return host{}, errHostNotFound
}

// getCoordinator returns the oldest node in the memberlist.
func (d *discovery) getCoordinator() host {
	members := d.getMembers()
	// That's not dangerous because memberlist includes the node's itself at least.
	return members[0]
}

// isCoordinator returns true if the caller is the coordinator node.
func (d *discovery) isCoordinator() bool {
	return d.getCoordinator().Name == d.name
}

// localNode is used to return the local Node
func (d *discovery) localNode() *memberlist.Node {
	return d.memberlist.LocalNode()
}

// shutdown will stop any background maintenance of network activity
// for this memberlist, causing it to appear "dead". A leave message
// will not be broadcasted prior, so the cluster being left will have
// to detect this node's shutdown using probing. If you wish to more
// gracefully exit the cluster, call Leave prior to shutting down.
//
// This method is safe to call multiple times.
func (d *discovery) shutdown() error {
	select {
	case <-d.done:
		return nil
	default:
	}
	close(d.done)
	// TODO: We may want to add a timeout for this.
	d.wg.Wait()
	return d.memberlist.Shutdown()
}

func (d *discovery) handleEvent(event memberlist.NodeEvent) {
	d.eventMutex.RLock()
	defer d.eventMutex.RUnlock()

	for _, ch := range d.eventSubscribers {
		if event.Node.Name == d.name {
			continue
		}
		if event.Event != memberlist.NodeUpdate {
			ch <- event
			continue
		}
		// Overwrite it. In olricdb, NodeUpdate evaluated as NodeLeave
		event.Event = memberlist.NodeLeave
		ch <- event
		// Create a Join event from copied event.
		cpy := event
		cpy.Event = memberlist.NodeJoin
		ch <- cpy
		continue
	}
}

func (d *discovery) eventLoop() {
	defer d.wg.Done()

	for {
		select {
		case event := <-d.eventsChan:
			d.handleEvent(event)
		case <-d.done:
			return
		}
	}
}

func (d *discovery) subscribeNodeEvents() chan memberlist.NodeEvent {
	d.eventMutex.Lock()
	defer d.eventMutex.Unlock()

	ch := make(chan memberlist.NodeEvent, eventChanCapacity)
	d.eventSubscribers = append(d.eventSubscribers, ch)
	return ch
}

// delegate is a struct which implements memberlist.Delegate interface.
type delegate struct {
	meta []byte
}

// newDelegate returns a new delegate instance.
func newDelegate(birthdate int64) (delegate, error) {
	mt := &NodeMetadata{
		Birthdate: birthdate,
	}
	var buf bytes.Buffer
	if err := gob.NewEncoder(&buf).Encode(mt); err != nil {
		return delegate{}, err
	}
	return delegate{
		meta: buf.Bytes(),
	}, nil
}

// NodeMeta is used to retrieve meta-data about the current node
// when broadcasting an alive message. It's length is limited to
// the given byte size. This metadata is available in the Node structure.
func (d delegate) NodeMeta(limit int) []byte {
	return d.meta
}

// NotifyMsg is called when a user-data message is received.
func (d delegate) NotifyMsg(data []byte) {}

// GetBroadcasts is called when user data messages can be broadcast.
func (d delegate) GetBroadcasts(overhead, limit int) [][]byte { return nil }

// LocalState is used for a TCP Push/Pull.
func (d delegate) LocalState(join bool) []byte { return nil }

// MergeRemoteState is invoked after a TCP Push/Pull.
func (d delegate) MergeRemoteState(buf []byte, join bool) {}
