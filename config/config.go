// Copyright (c) 2018 Burak Sezer
// All rights reserved.
//
// This code is licensed under the MIT License.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files(the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions :
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package config

import (
	"os"

	"github.com/BurntSushi/toml"
)

const (
	// DefaultConfigFile is the default configuration file path on a Unix-based operating system.
	DefaultConfigFile = "radyo.toml"

	// EnvConfigFile is the name of environment variable which can be used to override default configuration file path.
	EnvConfigFile = "RADYO_CONFIG"
)

type radyo struct {
	InternalAddr    string `toml:"internalAddr"`
	ClientAddr      string `toml:"clientAddr"`
	CertFile        string `toml:"certFile"`
	KeyFile         string `toml:"keyFile"`
	AuthCallbackUrl string `toml:"authCallbackUrl"`
	AuthTimeout     string `toml:"authTimeout"`
}

// logging contains configuration variables of logging section of config file.
type logging struct {
	Level           string `toml:"level"`
	TimeFieldFormat string `toml:"timeFieldFormat"`
}

type discovery struct {
	Environment         string   `toml:"environment"`
	Addr                string   `toml:"addr"`
	EnableCompression   bool     `toml:"enableCompression"`
	Peers               []string `toml:"peers"`
	IndirectChecks      int      `toml:"indirectChecks"`
	RetransmitMult      int      `toml:"retransmitMult"`
	SuspicionMult       int      `toml:"suspicionMult"`
	TCPTimeout          string   `toml:"tcpTimeout"`
	PushPullInterval    string   `toml:"pushPullInterval"`
	ProbeTimeout        string   `toml:"probeTimeout"`
	ProbeInterval       string   `toml:"probeInterval"`
	GossipInterval      string   `toml:"gossipInterval"`
	GossipToTheDeadTime string   `toml:"gossipToTheDeadTime"`
}

type httpClient struct {
	DialerTimeout      string `toml:"dialerTimeout"`
	InsecureSkipVerify bool   `toml:"insecureSkipVerify"`
	Timeout            string `toml:"timeout"`
}

// Config is the main configuration struct
type Config struct {
	HTTPClient httpClient
	Discovery  discovery
	Logging    logging
	Radyo      radyo
}

// New creates a new configuration object of Radyo
func New(path string) (*Config, error) {
	if len(path) == 0 {
		path = os.Getenv(EnvConfigFile)
	}
	if len(path) == 0 {
		path = DefaultConfigFile
	}
	var c Config
	if _, err := toml.DecodeFile(path, &c); err != nil {
		return nil, err
	}
	return &c, nil
}
