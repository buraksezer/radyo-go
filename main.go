// Copyright (c) 2018 Burak Sezer
// All rights reserved.
//
// This code is licensed under the MIT License.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files(the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions :
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"math/rand"
	"os"
	"runtime"
	"time"

	"github.com/buraksezer/radyo/config"
	"github.com/buraksezer/radyo/radyo"
	isatty "github.com/mattn/go-isatty"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

var usage = `Radyo is a distributed publish-subscribe server

Usage: 
  radyo [flags] ...

Flags:
  -h -help                      
      Shows this screen.

  -v -version                   
      Shows version information.

  -c -config                    
      Sets configuration file path. Default is /etc/radyo.toml. 
      Set RADYO_CONFIG to overwrite it.

The Go runtime version %s
Report bugs to https://github.com/buraksezer/radyo/issues`

const version = "0.1"

var (
	cpath              string
	showHelp           bool
	showVersion        bool
	generateIdentifier bool
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

func configureLogger(c *config.Config) {
	switch {
	case c.Logging.TimeFieldFormat == "ANSIC":
		zerolog.TimeFieldFormat = time.ANSIC
	case c.Logging.TimeFieldFormat == "UnixDate":
		zerolog.TimeFieldFormat = time.UnixDate
	case c.Logging.TimeFieldFormat == "RubyDate":
		zerolog.TimeFieldFormat = time.RubyDate
	case c.Logging.TimeFieldFormat == "RFC822":
		zerolog.TimeFieldFormat = time.RFC822
	case c.Logging.TimeFieldFormat == "RFC822Z":
		zerolog.TimeFieldFormat = time.RFC822Z
	case c.Logging.TimeFieldFormat == "RFC850":
		zerolog.TimeFieldFormat = time.RFC850
	case c.Logging.TimeFieldFormat == "RFC1123":
		zerolog.TimeFieldFormat = time.RFC1123
	case c.Logging.TimeFieldFormat == "RFC1123Z":
		zerolog.TimeFieldFormat = time.RFC1123Z
	case c.Logging.TimeFieldFormat == "RFC3339":
		zerolog.TimeFieldFormat = time.RFC3339
	case c.Logging.TimeFieldFormat == "RFC339":
		zerolog.TimeFieldFormat = time.RFC3339
	case c.Logging.TimeFieldFormat == "RFC3339Nano":
		zerolog.TimeFieldFormat = time.RFC3339Nano
	case c.Logging.TimeFieldFormat == "Kitchen":
		zerolog.TimeFieldFormat = time.Kitchen
	case c.Logging.TimeFieldFormat == "Stamp":
		zerolog.TimeFieldFormat = time.Stamp
	case c.Logging.TimeFieldFormat == "StampMilli":
		zerolog.TimeFieldFormat = time.StampMilli
	case c.Logging.TimeFieldFormat == "StampMicro":
		zerolog.TimeFieldFormat = time.StampMicro
	case c.Logging.TimeFieldFormat == "StampNano":
		zerolog.TimeFieldFormat = time.StampNano
	default:
		zerolog.TimeFieldFormat = ""
	}

	if isatty.IsTerminal(os.Stdout.Fd()) {
		log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})
	}

	switch {
	case c.Logging.Level == "panic":
		zerolog.SetGlobalLevel(zerolog.PanicLevel)
	case c.Logging.Level == "fatal":
		zerolog.SetGlobalLevel(zerolog.FatalLevel)
	case c.Logging.Level == "error":
		zerolog.SetGlobalLevel(zerolog.ErrorLevel)
	case c.Logging.Level == "warn":
		zerolog.SetGlobalLevel(zerolog.WarnLevel)
	case c.Logging.Level == "debug":
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	case c.Logging.Level == "info":
		zerolog.SetGlobalLevel(zerolog.InfoLevel)
	default:
		zerolog.SetGlobalLevel(zerolog.InfoLevel)
	}
}

func main() {
	// Parse command line parameters
	f := flag.NewFlagSet(os.Args[0], flag.ContinueOnError)
	f.SetOutput(ioutil.Discard)
	f.BoolVar(&showHelp, "h", false, "")
	f.BoolVar(&showHelp, "help", false, "")
	f.BoolVar(&showVersion, "version", false, "")
	f.BoolVar(&showVersion, "v", false, "")
	f.StringVar(&cpath, "config", config.DefaultConfigFile, "")
	f.StringVar(&cpath, "c", config.DefaultConfigFile, "")

	if err := f.Parse(os.Args[1:]); err != nil {
		log.Fatal().Err(err).Msg("Failed to parse flags")
	}

	if showVersion {
		fmt.Printf("Radyo %s with runtime %s\n", version, runtime.Version())
		return
	} else if showHelp {
		msg := fmt.Sprintf(usage, runtime.Version())
		fmt.Println(msg)
		return
	}

	c, err := config.New(cpath)
	if err != nil {
		log.Fatal().Err(err).Msg("Failed to parse config file")
	}
	configureLogger(c)
	hostname, err := os.Hostname()
	if err != nil {
		log.Fatal().Err(err).Msg("Failed to get hostname")
	}

	log.Info().Str("name", os.Args[0]).Str("version", version).Int("pid", os.Getpid()).Str("hostname", hostname).Msg("Running")
	r, err := radyo.New(c)
	if err != nil {
		log.Fatal().Err(err).Msg("Failed to create a new Radyo instance")
	}

	if err = r.Start(); err != nil {
		log.Fatal().Err(err).Msg("Radyo server returned an error")
	}
	log.Info().Msg("Good byte!")
}
